import { API_V1_URI, get, post, deleteAuth } from "./http";

export const getFile = async (id) => get(`${API_V1_URI}/file/${id}/`);

export const getFiles = async (id) => get(`${API_V1_URI}/file/`);

export const addFile = async (file) => post(`${API_V1_URI}/file/`, file);

export const removeFile = async (file_id) =>
  deleteAuth(`${API_V1_URI}/file/${file_id}/`);

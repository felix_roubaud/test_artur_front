import { API_V1_URI, API_URI, get, post, patch } from "./http";

export const fetchUser = async (id) => get(`${API_URI}/user/${id}`);

export const fetchUsers = async () => get(`${API_V1_URI}/user`);

export const fetchFriends = async () => get(`${API_V1_URI}/user/get_friends`);

export const addFriends = async (users_id) =>
  post(`${API_V1_URI}/user/add_friends/`, { users_id });

export const removeFriend = async (user_id) =>
  post(`${API_V1_URI}/user/remove_friend/`, { user_id });

export const searchUsers = async (username) =>
  get(`${API_V1_URI}/user?search=${username}`);

export const fetchMe = async () => get(`${API_V1_URI}/user/get_me/`);

// export const updateUser = async (data) => {
//   for (var field in data) {
//     console.log(field);
//     post(`${API_URI}/auth/users/set_${field}/`, {
//       [`new_${field}`]: data[field],
//     });
//   }
// };

export const updateUser = async (id, data) => {
  return patch(`${API_V1_URI}/user/${id}/`, data);
};

export const changePassword = async (data) => {
  return post(`${API_URI}/auth/users/set_password/`, data);
};

export const loginUser = async (username, password) =>
  post(`${API_URI}/auth/token/login/`, { username, password });

export async function updatePhoto(fileData) {
  return patch(`${API_V1_URI}/user/photo_upload/`, fileData);
}

export const createUser = async (data) => post(`${API_URI}/auth/users/`, data);

import axios from "axios";

export const API_V1_URI = "http://localhost:8000/api/v1";
export const API_URI = "http://localhost:8000";

const storage = window.localStorage;

// axios.interceptors.request.use(function (config) {
//   const token_ = getAuthToken();
//   config.headers.Authorization = token_;
//   return config;
// });

export function setAuthToken(token) {
  storage.setItem("http:token", token);
}

export function getAuthToken() {
  return storage.getItem("http:token");
}

export async function get(uri) {
  const token_ = getAuthToken();
  //   if (!token_ || token_ === "null") {
  //     return axios({ url: uri }, { method: "get" });
  //   }
  return axios({
    url: uri,
    method: "get",
    headers: { Authorization: `Token ${token_}` },
  });
}

export async function post(uri, data) {
  const token_ = getAuthToken();
  if (token_) {
    return axios.post(uri, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Token ${token_}`,
      },
    });
  }
  return axios.post(uri, data, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
}

export async function patch(uri, data) {
  const token_ = getAuthToken();
  if (token_) {
    return axios.patch(uri, data, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Token ${token_}`,
      },
    });
  }
  return axios.patch(uri, data, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
}

export async function deleteAuth(uri, data) {
  const token_ = getAuthToken();
  if (token_) {
    return axios.delete(uri, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Token ${token_}`,
      },
    });
  }
  return axios.delete(uri, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
}

// export async function delete(uri, data) {
//   const token_ = getAuthToken();
//   return axios.delete(uri, data, {
//     Accept: "application/json",
//     "Content-Type": "application/json",
//     Authorization: `Token ${token_}`,
//   });
// }

// axios.interceptors.response.use(
//   (res) => {
//     return res;
//   },
//   (err) => {
//     if (err?.response?.status === 401) {
//       window.localStorage.removeItem("http:token");
//       history.push("/login");
//     }
//     return err;
//   }
// );

import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Avatar, Button } from "@material-ui/core";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";

import UpdateUserInfoModal from "components/user/modals/UserUpdateInfoModal";
import ChangeUserPasswordModal from "components/user/modals/UserChangePasswordModal";
import ChangeUserPhotoModal from "components/user/modals/UserUpdatePhotoModal";
import UserPersonnalCard from "components/user/UserPersonnalCard";
import {
  updateUser as updateUserAPI,
  changePassword as changePasswordAPI,
  updatePhoto as updatePhotoAPI,
} from "api/userApi";
import { useSnackbar } from "context/snackbar";

function AccountPage({ user, fetchMe }) {
  const classes = useStyles();
  const { setSnackbar } = useSnackbar();

  const [openModal, setOpenModal] = useState(false);
  const [openModalPassword, setOpenModalPassword] = useState(false);
  const [openPhotoModal, setOpenPhotoModal] = useState(false);

  const [infosError, setInfosError] = useState(null);

  async function updateUser(data) {
    const response = await updateUserAPI(user.id, data);
    if (response && response.status >= 200 && response.status < 300) {
      fetchMe();
      setSnackbar({
        open: true,
        text: "Informations mises à jour",
      });
      setOpenModal(false);
    } else if (response?.response?.data) {
      setInfosError(Object.values(response.response.data));
    }
  }

  const [passwordError, setPasswordError] = useState(null);

  async function changePassword(data) {
    const response = await changePasswordAPI(data);
    if (response?.response?.data) {
      setPasswordError(Object.values(response.response.data));
    } else {
      setOpenModalPassword(false);
      setSnackbar({
        open: true,
        text: "Mot de passe modifié !",
      });
    }
  }

  async function updatePhoto(data) {
    const response = await updatePhotoAPI(data);
    if (response?.response?.data) {
      setSnackbar({
        open: true,
        text: "Aie !",
        error: true,
      });
    } else {
      fetchMe();
      setOpenPhotoModal(false);
      setSnackbar({
        open: true,
        text: "Photo mise à jour",
      });
    }
  }

  function renderUpdateButtons() {
    return (
      <div className={classes.buttonContainer}>
        <div
          onClick={() => setOpenModalPassword(true)}
          className={classes.aTag}
        >
          Changer le mot de passe
        </div>
        <Button
          variant="contained"
          color="primary"
          onClick={() => setOpenModal(true)}
          className={classes.button}
        >
          Modifier les informations
        </Button>
      </div>
    );
  }
  return (
    <>
      <Typography variant="h4" className={classes.title}>
        Mon profil
      </Typography>
      <div className={classes.container}>
        <div className={classes.infoContainer}>
          <div className={classes.leftContainer}>
            <Avatar
              src={`${process.env.REACT_APP_API_URI}${user?.photo}`}
              className={classes.photo}
            />
            <Button onClick={() => setOpenPhotoModal(true)}>
              <PhotoCameraIcon className={classes.icon} />
            </Button>
          </div>
          <div className={classes.rightContainer}>
            <UserPersonnalCard user={user} />
            {renderUpdateButtons()}
          </div>
        </div>
      </div>
      <UpdateUserInfoModal
        open={openModal}
        onClose={() => setOpenModal(false)}
        onConfirm={updateUser}
        user={user}
        error={infosError}
      />
      <ChangeUserPasswordModal
        open={openModalPassword}
        onClose={() => setOpenModalPassword(false)}
        onConfirm={changePassword}
        user={user}
        error={passwordError}
      />
      <ChangeUserPhotoModal
        open={openPhotoModal}
        onClose={() => setOpenPhotoModal(false)}
        onConfirm={updatePhoto}
        user={user}
      />
    </>
  );
}

const useStyles = makeStyles((theme) => ({
  rightContainer: {
    width: "50%",
  },
  aTag: {
    textDecoration: "underline",
    cursor: "pointer",
  },
  title: {
    marginBottom: theme.spacing(10),
  },
  buttonContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
  },
  leftContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  icon: {
    color: theme.palette.primary.main,
    fontSize: "50px",
  },
  photo: {
    width: "25vw",
    height: "25vw",
    marginBottom: theme.spacing(2),
  },
  button: {
    marginTop: theme.spacing(),
    padding: theme.spacing(),
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    padding: theme.spacing(5),
  },
  infoContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
  },
}));

export default AccountPage;

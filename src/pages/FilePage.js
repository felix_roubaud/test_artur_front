import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { Typography, Fab } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Skeleton from "@material-ui/lab/Skeleton";

import FileSummaryCard from "components/file/FileSummaryCard";
import FileCard from "components/file/FileCard";
import FileAddModal from "../components/file/FileAddModal";

import {
  addFile as addFileAPI,
  removeFile as removeFileAPI,
  getFiles as getFilesAPI,
  getFile as getFileAPI,
} from "api/fileApi";
import { useSnackbar } from "context/snackbar";

function FilePage(props) {
  const classes = useStyles();
  const history = useHistory();
  const { setSnackbar } = useSnackbar();

  const file_id = parseInt(props?.match?.params.id, 10) || null;
  const [selectedFile, setSelectedFile] = useState(null);
  useEffect(() => {
    async function getFile() {
      const response = await getFileAPI(file_id);
      if (
        response?.status &&
        response.status >= 200 &&
        response.status <= 300
      ) {
        var urlCreator = window.URL || window.webkitURL;
        var previewUrl = await urlCreator.createObjectURL(
          await new Blob([response.data], { type: "png" })
        );
        setSelectedFile(previewUrl);
      } else {
        setSnackbar({
          open: true,
          text: "Erreur lors de la récupération du document",
          error: true,
        });
      }
    }
    if (file_id) {
      getFile();
    }
  }, [file_id, setSnackbar]);

  const [openAddModal, setOpenAddModal] = useState(false);
  const [loading, setLoading] = useState(true);

  const [files, setFiles] = useState([]);
  async function getFiles() {
    const response = await getFilesAPI();
    if (response?.status && response.status >= 200 && response.status <= 300) {
      setFiles(response.data);
      setLoading(false);
    }
  }
  useEffect(() => {
    getFiles();
  }, []);

  async function addFile(formData) {
    const response = await addFileAPI(formData);
    if (response?.status && response.status >= 200 && response.status <= 300) {
      setSnackbar({ open: true, text: "Document ajouté" });
      setOpenAddModal(false);
      getFiles();
    } else {
      setSnackbar({
        open: true,
        text: "Erreur lors de l'ajout du document",
        error: true,
      });
    }
  }

  async function removeFile(file_id) {
    const response = await removeFileAPI(file_id);
    if (response?.status && response.status >= 200 && response.status <= 300) {
      setSnackbar({ open: true, text: "Document supprimé" });
      setFiles((prevFiles) => prevFiles.filter((f) => f.id !== file_id));
    } else {
      setSnackbar({
        open: true,
        text: "Erreur lors de la suppression du document",
        error: true,
      });
    }
  }
  return (
    <>
      <Typography variant="h4" className={classes.title}>
        Mes documents
      </Typography>
      <div className={classes.container}>
        <div className={classes.fileList}>
          {loading &&
            [1, 2, 3, 4, 5, 6].map(() => (
              <Skeleton className={classes.skeleton} />
            ))}
          {files.map((f) => (
            <FileSummaryCard
              divider
              key={f.id}
              file={f}
              selected={f.id === file_id}
              onClick={() => history.push(`/files/${f.id}`)}
            />
          ))}
        </div>
        <FileCard
          file={files.find((f) => f.id === file_id)}
          fileContent={selectedFile}
          onRemove={() => removeFile(file_id)}
        />
        <FileAddModal
          onClose={() => setOpenAddModal(false)}
          open={openAddModal}
          onConfirm={addFile}
        />
        <Fab
          color="primary"
          aria-label="add"
          onClick={() => setOpenAddModal(true)}
          className={classes.fab}
        >
          <AddIcon />
        </Fab>
      </div>
    </>
  );
}

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "fixed",
    right: "20px",
    bottom: "20px",
  },
  skeleton: {
    width: "100%",
    height: "250px",
  },
  fileList: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr",
    gridRowGap: "50px",
    gridColumnGap: "50px",
    width: "49%",
    maxHeight: "80vh",
    overflowY: "auto",
    paddingBottom: "50px",
  },
  title: {
    marginBottom: theme.spacing(2),
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
}));

export default FilePage;

import React, { useState, useEffect, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { Typography, Fab } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Skeleton from "@material-ui/lab/Skeleton";

import UserListItem from "components/user/UserListItem";
import UserCard from "components/user/UserCard";
import {
  fetchFriends,
  removeFriend as removeFriendAPI,
  searchUsers as searchUsersAPI,
  addFriends as addFriendsAPI,
} from "api/userApi";
import UserInviteModal from "../components/user/modals/UserInviteModal";
import { useSnackbar } from "context/snackbar";

function FriendPage(props) {
  const classes = useStyles();
  const history = useHistory();
  const { setSnackbar } = useSnackbar();

  const friend_id = parseInt(props?.match?.params.id, 10) || null;

  const [openInviteModal, setOpenInviteModal] = useState(false);
  const [loading, setLoading] = useState(true);

  const [friends, setFriends] = useState([]);

  useEffect(() => {
    async function getFriends() {
      const response = await fetchFriends();
      if (
        response?.status &&
        response.status >= 200 &&
        response.status <= 300
      ) {
        setFriends(response.data);
        setLoading(false);
      }
    }
    getFriends();
  }, [setFriends, setLoading]);

  async function addFriends(newFriends) {
    const response = await addFriendsAPI(newFriends.map((u) => u.id));
    if (response?.status && response.status >= 200 && response.status <= 300) {
      setFriends([...friends, ...newFriends]);
      setSearchedUsers(
        searchedUsers.filter(
          (u) =>
            ![...friends, ...newFriends].find((f) => f.id === u.id) &&
            u.id !== props?.user?.id
        )
      );
      setSnackbar({ open: true, text: "Amis ajoutés" });
    } else {
      setSnackbar({
        open: true,
        text: "Erreur lors de l'ajout",
        error: true,
      });
    }
  }

  async function removeFriend(user_id) {
    const response = await removeFriendAPI(user_id);
    if (response?.status && response.status >= 200 && response.status <= 300) {
      setFriends(friends.filter((u) => u.id !== user_id));
      setSnackbar({ open: true, text: "Ami supprimé" });
    } else {
      setSnackbar({
        open: true,
        text: "Erreur lors de la suppression",
        error: true,
      });
    }
  }

  const [searchedUsers, setSearchedUsers] = useState([]);

  useEffect(() => {
    setSearchedUsers((prevSearchedUsers) =>
      prevSearchedUsers.filter(
        (u) => !friends.find((f) => f.id === u.id) && u.id !== props?.user?.id
      )
    );
  }, [friends, setSearchedUsers, props]);

  const searchUsers = useCallback(
    async (username) => {
      const response = await searchUsersAPI(username);
      if (response && response.status >= 200 && response.status <= 300) {
        setSearchedUsers(
          response.data.results.filter(
            (u) =>
              !friends.find((f) => f.id === u.id) && u.id !== props?.user?.id
          )
        );
      }
    },
    [friends, props]
  );

  return (
    <>
      <Typography variant="h4" className={classes.title}>
        Mes amis
      </Typography>
      <div className={classes.container}>
        <div className={classes.friendList}>
          {loading &&
            [1, 2, 3].map(() => <Skeleton className={classes.skeleton} />)}
          {friends.map((user) => (
            <UserListItem
              divider
              key={user.id}
              user={user}
              selected={user.id === friend_id}
              onClick={() => history.push(`/friends/${user.id}`)}
            />
          ))}
        </div>
        <UserCard
          user={friends.find((user) => user.id === friend_id)}
          onRemove={() => removeFriend(friend_id)}
        />
        <UserInviteModal
          onClose={() => setOpenInviteModal(false)}
          searchUsers={searchUsers}
          users={searchedUsers}
          open={openInviteModal}
          onConfirm={addFriends}
        />
        <Fab
          color="primary"
          aria-label="add"
          onClick={() => setOpenInviteModal(true)}
          className={classes.fab}
        >
          <AddIcon />
        </Fab>
      </div>
    </>
  );
}

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "fixed",
    right: "20px",
    bottom: "20px",
  },
  skeleton: {
    width: "100%",
    height: "75px",
  },
  friendList: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "49%",
    height: "80vh",
    overflowY: "auto",
  },
  title: {
    marginBottom: theme.spacing(2),
  },
  container: {
    display: "flex",
    justifyContent: "space-between",
    width: "100%",
  },
}));

export default FriendPage;

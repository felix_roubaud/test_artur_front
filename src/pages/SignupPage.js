import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import SingupForm from "../components/user/UserSignupForm";
import {
  createUser as createUserAPI,
  loginUser as loginUserAPI,
} from "api/userApi";
import { makeStyles } from "@material-ui/core/styles";
import { useAuth } from "../context/auth";
import { ReactComponent as Svg } from "../components/utils/loginImage.svg";

function SignupPage({ fetchMe }) {
  const classes = useStyles();
  const { setAuthTokens } = useAuth();
  let history = useHistory();

  const [signupError, setSignupError] = useState(null);
  async function signup(username, password) {
    const response = await createUserAPI({ username, password });
    if (response?.response?.data) {
      setSignupError(Object.values(response.response.data));
    } else {
      const loginResponse = await loginUserAPI(username, password);
      setAuthTokens(loginResponse.data.auth_token);
      fetchMe();
      history.push("/account");
    }
  }

  return (
    <div className={classes.container}>
      <SingupForm signupUser={signup} error={signupError} />
      <div className={classes.rightPanel}>
        <Svg className={classes.svg} />
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    background: theme.palette.primary.light,
  },
  svg: {
    maxWidth: "600px",
  },
  rightPanel: {
    width: "50%",
    height: "100vh",
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

export default SignupPage;

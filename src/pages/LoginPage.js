import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import LoginForm from "../components/user/UserLoginForm";
import { loginUser as loginUserAPI } from "api/userApi";
import { makeStyles } from "@material-ui/core/styles";
import { useAuth } from "../context/auth";
import { ReactComponent as Svg } from "../components/utils/loginImage.svg";

function LoginPage({ fetchMe }) {
  const classes = useStyles();
  const { setAuthTokens } = useAuth();
  let history = useHistory();

  const [loginError, setLoginError] = useState(null);
  async function login(username, password) {
    const response = await loginUserAPI(username, password);
    if (response?.response?.data) {
      setLoginError(Object.values(response.response.data));
    } else {
      setAuthTokens(response.data.auth_token);
      fetchMe();
      history.push("/account");
    }
  }

  return (
    <div className={classes.container}>
      <LoginForm loginUser={login} error={loginError} />
      <div className={classes.rightPanel}>
        <Svg className={classes.svg} />
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    background: theme.palette.primary.light,
  },
  svg: {
    maxWidth: "600px",
  },
  rightPanel: {
    width: "50%",
    height: "100vh",
    backgroundColor: theme.palette.primary.main,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

export default LoginPage;

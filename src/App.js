import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { ThemeProvider } from "@material-ui/core/styles";

import "./App.css";
import Root from "./Rooter";
import Authorisation from "context/auth";
import Snackbar from "context/snackbar";
import theme from "./theme";

function App() {
  return (
    <Snackbar>
      <ThemeProvider theme={theme}>
        <Router className="App">
          <Authorisation>
            <Root />
          </Authorisation>
        </Router>
      </ThemeProvider>
    </Snackbar>
  );
}

export default App;

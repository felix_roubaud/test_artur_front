import React, { useState, useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { fetchMe as fetchMeAPI } from "api/userApi";
import AccountPage from "./pages/AccountPage";
import PrivateRoute from "./PrivateRoute";
import LoginPage from "./pages/LoginPage";
import SignupPage from "./pages/SignupPage";
import FriendPage from "./pages/FriendPage";
import FilePage from "./pages/FilePage";
import Template from "./components/template/Template";

function Roots() {
  const [user, setUser] = useState({});

  useEffect(() => {
    fetchMeAPI().then((res) => setUser(res.data));
  }, []);

  function fetchMe() {
    fetchMeAPI().then((res) => setUser(res.data));
  }

  return (
    <>
      <Switch>
        <Route
          exact
          path="/login"
          render={(props) => <LoginPage {...props} fetchMe={fetchMe} />}
        />
        <Route
          exact
          path="/signup"
          render={(props) => <SignupPage {...props} fetchMe={fetchMe} />}
        />
        <Route path="/">
          <Template user={user}>
            <Switch>
              <Route path="/template" component={Template} />
              <Route
                exact
                path="/friends/:id"
                render={(props) => <FriendPage {...props} user={user} />}
              />
              <Route
                exact
                path="/friends"
                render={(props) => <FriendPage {...props} user={user} />}
              />
              <PrivateRoute path="/account">
                <AccountPage user={user} fetchMe={fetchMe} />
              </PrivateRoute>
              <PrivateRoute exact path="/files/:id" component={FilePage} />
              <PrivateRoute exact path="/files">
                <FilePage />
              </PrivateRoute>
              <Route path="/">
                <Redirect to="/account" />
              </Route>
            </Switch>
          </Template>
        </Route>
      </Switch>
    </>
  );
}

export default Roots;

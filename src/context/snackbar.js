import React, { createContext, useContext, useReducer } from "react";
import { Snackbar, SnackbarContent } from "@material-ui/core";

export const SnackbarContext = createContext();

export function useSnackbar() {
  return useContext(SnackbarContext);
}

function SnackbarCustom(props) {
  const [state, setState] = useReducer(
    (prevState, payload) => {
      return { ...payload };
    },
    { open: false, text: "", error: false }
  );
  return (
    <>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={state.open}
        autoHideDuration={4000}
        onClose={() =>
          setState((prevState) => {
            return { ...prevState, open: false };
          })
        }
      >
        <SnackbarContent
          style={{
            backgroundColor: state.error ? "#f74336" : "rgb(33, 116, 138)",
          }}
          message={
            <span style={{ fontWeight: "bold", letterSpacing: 0.8 }}>
              {state.text}
            </span>
          }
        />
      </Snackbar>

      <SnackbarContext.Provider value={{ setSnackbar: setState }}>
        {props.children}
      </SnackbarContext.Provider>
    </>
  );
}

export default SnackbarCustom;

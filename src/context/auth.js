import React, { useState, createContext, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

const storage = window.localStorage;

export const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}

function Authorisation(props) {
  let token = storage.getItem("http:token");
  if (!token) {
    token = null;
  }
  const history = useHistory();

  // disconnect user every time he is not authenticated with a correct token
  axios.interceptors.response.use(
    (res) => {
      return res;
    },
    (err) => {
      console.log(err.response);

      if (err?.response?.status === 401) {
        window.localStorage.removeItem("http:token");
        history.push("/login");
      }
      return err;
    }
  );
  const [authTokens, setAuthTokens] = useState(token);

  const setTokens = (data) => {
    if (data) {
      storage.setItem("http:token", data);
    } else {
      storage.removeItem("http:token");
    }
    setAuthTokens(data);
  };
  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      {props.children}
    </AuthContext.Provider>
  );
}

export default Authorisation;

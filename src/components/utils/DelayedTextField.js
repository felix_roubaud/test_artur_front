import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { TextField } from "@material-ui/core";

const DELAY = 350;

function DelayedTextField(props) {
  const [value, setValue] = useState(props.value);
  const [writingSince, setWritingSince] = useState(null);

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  function getDelay() {
    return props.delay || DELAY;
  }

  function handleChange(e) {
    e.persist();
    setWritingSince(Date.now());
    setValue(e.target.value);
    setTimeout(sendChange(e), getDelay() + 10);
  }

  function sendChange(e) {
    return () => {
      if (
        (!writingSince || Date.now() - writingSince > getDelay()) &&
        e.target.value !== props.value
      ) {
        props.onChange(e);
      }
    };
  }
  return (
    <TextField
      {...props}
      value={value}
      onChange={(event) => handleChange(event)}
    />
  );
}

DelayedTextField.propTypes = {
  value: PropTypes.string,
  delay: PropTypes.number,
  onChange: PropTypes.func,
};

export default DelayedTextField;

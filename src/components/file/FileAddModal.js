import React, { useState, useCallback, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  Typography,
  DialogContent,
  Paper,
  TextField,
} from "@material-ui/core";
import { useDropzone } from "react-dropzone";

function FileAddModal({ open, onClose, onConfirm, error }) {
  const classes = useStyles();
  const [documentPreview, setDocumentPreview] = useState(null);
  const [file, setFile] = useState(null);
  const [title, setTitle] = useState("");
  const [baseName, setBaseName] = useState("");
  const [description, setDescription] = useState("");

  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      setBaseName(file.name);
      setFile(file);
      const reader = new FileReader();
      reader.onabort = () => console.log("file reading was aborted");
      reader.onerror = () => console.log("file reading has failed");
      reader.onload = async () => {
        const binaryStr = reader.result;
        var urlCreator = window.URL || window.webkitURL;
        var imageURL = await urlCreator.createObjectURL(
          await new Blob([binaryStr])
        );
        setDocumentPreview(imageURL);
      };
      reader.readAsArrayBuffer(file);
    });
  }, []);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  function closeModal() {
    setTitle("");
    setDescription("");
    setBaseName("");
    setFile(null);
    setDocumentPreview(null);
    onClose();
  }

  useEffect(() => {
    setTitle("");
    setDescription("");
    setBaseName("");
    setFile(null);
    setDocumentPreview(null);
  }, [open]);

  return (
    <Dialog open={open} onClose={closeModal}>
      <DialogTitle>Ajouter un document</DialogTitle>
      <DialogContent>
        <div className={classes.content}>
          <TextField
            placeholder="Titre"
            onChange={(ev) => setTitle(ev.target.value)}
            value={title}
            className={classes.textField}
          />
          <TextField
            placeholder="Description"
            onChange={(ev) => setDescription(ev.target.value)}
            value={description}
            multiline
            rows={4}
            className={classes.textField}
          />
          {documentPreview ? (
            <Typography className={classes.preview}>{baseName}</Typography>
          ) : (
            <div {...getRootProps()} className={classes.dragContainer}>
              <input {...getInputProps()} />
              {isDragActive ? (
                <p>Glisse le fichier ici ...</p>
              ) : (
                <Paper className={classes.infoContainer}>
                  <Typography align="center">
                    Glisse ton document ici, ou clique pour sélectionner un
                    fichier
                  </Typography>
                </Paper>
              )}
            </div>
          )}
        </div>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={closeModal} color="primary">
          Annuler
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={!file || !title}
          onClick={async () => {
            var formData = new FormData();
            formData.append("file", file);
            formData.append("title", title);
            formData.append("baseName", baseName);
            formData.append("description", description);
            onConfirm(formData);
          }}
        >
          Confirmer
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyles = makeStyles((theme) => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
  content: {
    display: "flex",
    flexDirection: "column",
    width: "300px",
    height: "300px",
  },
  preview: {
    marginTop: theme.spacing(3),
  },
  verticalContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  switchIcon: {
    color: theme.palette.primary.main,
    fontSize: "50px",
    marginTop: theme.spacing(3),
  },
  dragContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  infoContainer: {
    width: "70%",
    padding: theme.spacing(2),
  },
}));

export default FileAddModal;

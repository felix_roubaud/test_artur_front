import React from "react";
import { Typography, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PDF from "../utils/pdf.png";
import Picture from "../utils/picture.png";
import { ReactComponent as Word } from "../utils/word.svg";
import { ReactComponent as Excel } from "../utils/excel.svg";

function FileSummaryCard({ file, onClick, selected }) {
  const classes = useStyles();

  function renderIcon() {
    if (
      file?.basename.includes("csv") ||
      file?.basename.includes("xls") ||
      file?.basename.includes("xlsx")
    ) {
      return <Excel className={classes.icon} />;
    }
    if (file?.basename.includes("pdf")) {
      return <img alt="pdf" src={PDF} className={classes.icon} />;
    }
    if (file?.basename.includes("docx") || file?.basename.includes("odt")) {
      return <Word className={classes.icon} />;
    }
    if (file?.basename.includes("pptx")) {
      return <Excel className={classes.icon} />;
    } else {
      return <img src={Picture} alt="im" className={classes.icon} />;
    }
  }
  return (
    <Paper
      className={`${classes.paper} ${selected ? classes.selected : null}`}
      onClick={onClick}
    >
      <div className={classes.container}>
        {renderIcon()}
        <Typography className={classes.title}>{file?.title} </Typography>
      </div>
    </Paper>
  );
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end",
    margin: theme.spacing(2),
    height: "80%",
  },
  selected: {
    backgroundColor: theme.palette.primary.light,
  },
  icon: {
    width: "120px",
    height: "120px",
  },
  paper: {
    height: "200px",
    cursor: "pointer",
  },
  title: {
    marginTop: theme.spacing(2),
  },
}));

export default FileSummaryCard;

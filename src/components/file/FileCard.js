import React, { useState } from "react";
import {
  Typography,
  Paper,
  Button,
  DialogContent,
  Dialog,
  DialogActions,
  DialogTitle,
} from "@material-ui/core";
import FileViewer from "react-file-viewer";
import { makeStyles } from "@material-ui/core/styles";
import { ReactComponent as EmptyIcon } from "../utils/waiting.svg";
import { CustomErrorComponent } from "custom-error";

function UserCard({ file, onRemove, fileContent }) {
  const classes = useStyles();
  const [opensConfirmModal, setOpenConfirmModal] = useState(false);

  function renderConfirmModal() {
    return (
      <Dialog
        open={opensConfirmModal}
        onClose={() => setOpenConfirmModal(false)}
      >
        <DialogTitle>Confirmation</DialogTitle>
        <DialogContent>{`Es tu sûr de vouloir supprimer ${file?.title} de tes documents ?`}</DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={() => setOpenConfirmModal(false)}
            color="primary"
          >
            Annuler
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setOpenConfirmModal(false);
              onRemove();
            }}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  if (file) {
    return (
      <div className={classes.container}>
        <Paper className={classes.cardContainer}>
          <div className={classes.infoContainer}>
            <Typography className={classes.field} variant="h4">
              {file?.title}
            </Typography>
            <Typography className={classes.field}>
              {file?.description}
            </Typography>
            <FileViewer
              fileType={file?.type}
              filePath={file?.document}
              errorComponent={CustomErrorComponent}
              // onError={this.onError}
            />
            <Typography className={classes.field}>{file?.basename}</Typography>
          </div>
        </Paper>
        <Button
          variant="outlined"
          color="secondary"
          onClick={() => setOpenConfirmModal(true)}
          className={classes.button}
        >
          Retirer de mes documents
        </Button>
        {renderConfirmModal()}
      </div>
    );
  }
  return (
    <div className={classes.container}>
      <Paper className={classes.cardContainerEmpty}>
        <EmptyIcon className={classes.svg} />
        <Typography className={classes.emptyText} variant="subtitle1">
          Aucun document séléctionné
        </Typography>
      </Paper>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(),
  },
  field: {
    marginBottom: theme.spacing(),
    marginTop: theme.spacing(),
  },
  svg: {
    width: "150px",
    fill: "grey",
    opacity: 0.5,
    marginBottom: theme.spacing(1),
  },
  emptyText: {
    color: "grey",
    fontWeight: "bold",
  },
  infoContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-around",
  },

  cardContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: "5%",
    height: "300px",
    background: "#ececec",

    width: "95%",
  },
  cardContainerEmpty: {
    display: "flex",
    background: "#ececec",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    paddingBottom: "25px",
    paddingTop: "25px",
    height: "250px",
    width: "95%",
  },

  avatar: {
    width: "12vw",
    height: "12vw",
    marginRight: theme.spacing(10),
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    width: "50%",
  },
}));

export default UserCard;

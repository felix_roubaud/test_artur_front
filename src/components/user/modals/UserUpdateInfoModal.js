import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  TextField,
  Typography,
} from "@material-ui/core";

function UserUpdateInfoModal({ open, onClose, onConfirm, user, error }) {
  const classes = useStyles();
  const [userInfos, setUserInfos] = useState({
    username: user?.username,
    email: user?.email,
    first_name: user?.first_name,
    last_name: user?.last_name,
  });

  useEffect(() => {
    setUserInfos({
      username: user?.username,
      email: user?.email,
      first_name: user?.first_name,
      last_name: user?.last_name,
    });
  }, [setUserInfos, user, open]);

  function onChange(field) {
    return (ev) => {
      ev.persist();
      setUserInfos((prevInfos) => {
        return { ...prevInfos, [field]: ev.target.value };
      });
    };
  }
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Modifier mes informations</DialogTitle>
      <div className={classes.content}>
        <TextField
          placeholder="Nom d'utilisateur"
          value={userInfos.username}
          onChange={onChange("username")}
          className={classes.textField}
        />
        <TextField
          placeholder="Email"
          value={userInfos.email}
          onChange={onChange("email")}
          className={classes.textField}
        />
        <TextField
          placeholder="Prénom"
          value={userInfos.first_name}
          onChange={onChange("first_name")}
          className={classes.textField}
        />
        <TextField
          placeholder="Nom"
          value={userInfos.last_name}
          onChange={onChange("last_name")}
          className={classes.textField}
        />
        <div>
          {error &&
            error.map((err) => (
              <Typography color="secondary">{err}</Typography>
            ))}
        </div>
      </div>
      <DialogActions>
        <Button autoFocus onClick={onClose} color="primary">
          Annuler
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            onConfirm(userInfos);
          }}
        >
          Confirmer
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyles = makeStyles((theme) => ({
  content: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gridColumnGap: theme.spacing(4),
    gridRowGap: theme.spacing(2),
    margin: theme.spacing(3),
  },
  textField: {},
}));

export default UserUpdateInfoModal;

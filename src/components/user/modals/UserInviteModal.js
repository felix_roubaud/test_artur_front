import React, { useState, useEffect, useReducer } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  Checkbox,
  Typography,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import DelayedTextField from "../../utils/DelayedTextField";
import UserListItem from "../UserListItem";

function UserInviteModal({ open, onClose, onConfirm, users, searchUsers }) {
  const classes = useStyles();
  const [searchedText, setSearchedText] = useState("");
  const [selectedUsers, setSelectedUsers] = useReducer((prevState, list) => {
    return [...list];
  }, []);

  useEffect(() => {
    searchUsers("");
  }, [open, searchUsers]);

  function displayUsers(users, friend = false) {
    return (
      <div className={friend ? classes.firendList : classes.userList}>
        {users.map((user) => {
          return (
            <div
              key={user.id}
              className={classes.userContainer}
              onClick={() => {
                if (selectedUsers.find((u) => u.id === user.id)) {
                  setSelectedUsers(
                    selectedUsers.filter((u) => u.id !== user.id)
                  );
                } else {
                  selectedUsers.push(user);
                  setSelectedUsers(selectedUsers);
                }
              }}
            >
              <UserListItem user={user} />
              <Checkbox
                checked={selectedUsers.find((u) => u.id === user.id)}
                inputProps={{ "aria-label": "primary checkbox" }}
              />
            </div>
          );
        })}
      </div>
    );
  }

  function closeModal() {
    onClose();
    setSearchedText("");
    setSelectedUsers([]);
  }
  return (
    <Dialog open={open} onClose={closeModal}>
      <DialogTitle>Ajouter des amis</DialogTitle>
      <div className={classes.content}>
        <DelayedTextField
          value={searchedText}
          className={classes.searchField}
          placeholder="Trouver un utilisateur"
          onChange={(e) => {
            setSearchedText(e.target.value);
            searchUsers(e.target.value);
          }}
        />
        <ExpansionPanel>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography>{`${selectedUsers.length} amis sélectionnés`}</Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {displayUsers(selectedUsers, true)}
          </ExpansionPanelDetails>
        </ExpansionPanel>
        {displayUsers(
          users.filter((u) => {
            if (selectedUsers.find((us) => us.id === u.id)) {
              return false;
            }
            return true;
          }),
          false
        )}
      </div>
      <DialogActions>
        <Button onClick={closeModal} color="primary">
          Annuler
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            onConfirm(selectedUsers);
            closeModal();
          }}
        >
          Confirmer
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    flexDirection: "column",
    margin: theme.spacing(3),
    minWidth: "400px",
  },
  textField: {},
  userContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  searchField: {
    marginBottom: theme.spacing(2),
  },
  userList: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "300px",
    overflowY: "auto",
  },
  friendList: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
}));

export default UserInviteModal;

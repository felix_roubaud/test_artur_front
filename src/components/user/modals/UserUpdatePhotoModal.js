import React, { useState, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  Typography,
  DialogContent,
  Paper,
  Avatar,
} from "@material-ui/core";
import { useDropzone } from "react-dropzone";
import SwapHorizIcon from "@material-ui/icons/SwapHoriz";

function UserChangePhotoModal({ open, onClose, onConfirm, error, user }) {
  const classes = useStyles();
  const [photoPreview, setPhotoPreview] = useState(null);
  const [file, setFile] = useState(null);

  const onDrop = useCallback(
    (acceptedFiles) => {
      acceptedFiles.forEach((file) => {
        console.log(file, user);
        file.filename = `${user.id}.${file.filename}`;
        setFile(file);
        const reader = new FileReader();
        reader.onabort = () => console.log("file reading was aborted");
        reader.onerror = () => console.log("file reading has failed");
        reader.onload = async () => {
          const binaryStr = reader.result;
          var urlCreator = window.URL || window.webkitURL;
          var imageURL = await urlCreator.createObjectURL(
            await new Blob([binaryStr])
          );
          setPhotoPreview(imageURL);
        };
        reader.readAsArrayBuffer(file);
      });
    },
    [user]
  );
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Modifier ma photo</DialogTitle>
      <DialogContent>
        <div className={classes.content}>
          {photoPreview ? (
            <div className={classes.verticalContainer}>
              <Avatar
                src={photoPreview}
                style={{
                  borderRadius: 500,
                  boxShadow: "0px 2px 4px rgba(0,0,0,0.25)",
                  width: 300,
                  height: 300,
                }}
              />

              <SwapHorizIcon
                onClick={() => {
                  setPhotoPreview(null);
                }}
                className={classes.switchIcon}
              />
            </div>
          ) : (
            <div {...getRootProps()} className={classes.dragContainer}>
              <input {...getInputProps()} />
              {isDragActive ? (
                <p>Glisse le fichier ici ...</p>
              ) : (
                <Paper className={classes.infoContainer}>
                  <Typography align="center">
                    Glisse ta photo ici, ou clique pour sélectionner un fichier
                  </Typography>
                </Paper>
              )}
            </div>
          )}
        </div>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onClose} color="primary">
          Annuler
        </Button>
        <Button
          variant="contained"
          color="primary"
          onClick={async () => {
            var formData = new FormData();
            formData.append("file", file);
            onConfirm(formData);
          }}
        >
          Confirmer
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyles = makeStyles((theme) => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
  content: {
    display: "flex",
    flexDirection: "column",
  },
  verticalContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  switchIcon: {
    color: theme.palette.primary.main,
    fontSize: "50px",
    marginTop: theme.spacing(3),
  },
  dragContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  infoContainer: {
    width: "70%",
    padding: theme.spacing(2),
  },
}));

export default UserChangePhotoModal;

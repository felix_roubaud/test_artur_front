import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  TextField,
  DialogContent,
  Typography,
} from "@material-ui/core";

function UserChangePasswordModal({ open, onClose, onConfirm, error }) {
  const classes = useStyles();
  const [password, setPassword] = useState(null);
  const [newPassword, setNewPassword] = useState(null);
  const [passwordConfirmation, setPasswordConfirmation] = useState(null);
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>Modifier mon mot de passe</DialogTitle>
      <DialogContent>
        <div className={classes.content}>
          <TextField
            placeholder="Mot de passe actuel"
            value={password}
            onChange={(ev) => setPassword(ev.target.value)}
            className={classes.textField}
          />
          <TextField
            placeholder="Nouveau mot de passe"
            value={newPassword}
            onChange={(ev) => setNewPassword(ev.target.value)}
            className={classes.textField}
          />
          <TextField
            placeholder="Confirmation"
            value={passwordConfirmation}
            onChange={(ev) => setPasswordConfirmation(ev.target.value)}
            className={classes.textField}
          />
          {error &&
            error.map((err) => (
              <Typography color="secondary">{err}</Typography>
            ))}
        </div>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onClose} color="primary">
          Annuler
        </Button>
        <Button
          variant="contained"
          color="primary"
          disabled={!newPassword || newPassword !== passwordConfirmation}
          onClick={() => {
            onConfirm({
              current_password: password,
              new_password: newPassword,
            });
          }}
        >
          Confirmer
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const useStyles = makeStyles((theme) => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
  content: {
    display: "flex",
    flexDirection: "column",
  },
}));

export default UserChangePasswordModal;

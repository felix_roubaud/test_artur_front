import React, { useState, useRef, useEffect } from "react";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LockIcon from "@material-ui/icons/Lock";
import { Button, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

function UserSignupForm({ signupUser, error }) {
  let history = useHistory();
  const classes = useStyles();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const userNameRef = useRef();
  const loginButttonRef = useRef();
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");

  const [errorField, setErrorField] = useState(null);
  useEffect(() => {
    setErrorField(error);
  }, [error]);

  async function signUp() {
    if (passwordConfirm !== password) {
      setErrorField(["Password confirmation doesn't match password"]);
    } else {
      signupUser(username, password, passwordConfirm);
    }
  }

  function keyPress(e) {
    if (e.keyCode === 13 && !!username && !!password && !!passwordConfirm) {
      loginButttonRef.current.focus();
    } else if (e.keyCode === 13 && !!username && !password) {
      passwordRef.current.focus();
    } else if (e.keyCode === 13 && !!username && !!password) {
      passwordConfirmRef.current.focus();
    }
  }

  return (
    <div className={classes.container}>
      <TextField
        value={username}
        inputRef={userNameRef}
        className={classes.textField}
        autoFocus
        onKeyDown={keyPress}
        onChange={(ev) => {
          setErrorField(null);
          setUserName(ev.target.value);
        }}
        variant="outlined"
        placeholder="Nom d'utilisateur"
        icon={<AccountCircleIcon />}
        required
      />

      <TextField
        className={classes.textField}
        value={password}
        variant="outlined"
        onKeyDown={keyPress}
        inputRef={passwordRef}
        onChange={(ev) => {
          setPassword(ev.target.value);
          setErrorField(null);
        }}
        placeholder="Mot de passe"
        icon={<LockIcon />}
        required
        password
      />
      <TextField
        className={classes.textField}
        value={passwordConfirm}
        variant="outlined"
        onKeyDown={keyPress}
        inputRef={passwordConfirmRef}
        onChange={(ev) => {
          setPasswordConfirm(ev.target.value);
          setErrorField(null);
        }}
        placeholder="Confirmation"
        icon={<LockIcon />}
        required
        password
      />
      {errorField &&
        errorField.map((err) => (
          <Typography color="secondary">{err}</Typography>
        ))}
      <Button
        variant="contained"
        className={classes.login}
        onClick={signUp}
        color="primary"
        ref={loginButttonRef}
        disabled={!username || !password || !passwordConfirm}
      >
        Sign up
      </Button>
      <div className={classes.a} onClick={() => history.push("/login")}>
        Déjà inscrit ?
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  login: {
    padding: theme.spacing(),
    width: "100%",
    backgroundColor: theme.palette.primary,
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  textField: {
    marginBottom: theme.spacing(2),
    width: "100%",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "200px",
    marginRight: "120px",
  },
  a: {
    textDecoration: "underline",
    cursor: "pointer",
  },
}));

export default UserSignupForm;

import React, { useState } from "react";
import {
  Typography,
  Avatar,
  Paper,
  Button,
  DialogContent,
  Dialog,
  DialogActions,
  DialogTitle,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { ReactComponent as EmptyIcon } from "../utils/waiting.svg";

function UserCard({ user, onRemove }) {
  const classes = useStyles();
  const [opensConfirmModal, setOpenConfirmModal] = useState(false);

  function renderConfirmModal() {
    return (
      <Dialog
        open={opensConfirmModal}
        onClose={() => setOpenConfirmModal(false)}
      >
        <DialogTitle>Confirmation</DialogTitle>
        <DialogContent>{`Es tu sûr de vouloir retirer ${user.username} de tes amis ?`}</DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={() => setOpenConfirmModal(false)}
            color="primary"
          >
            Annuler
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              setOpenConfirmModal(false);
              onRemove();
            }}
          >
            Confirmer
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  if (user) {
    return (
      <div className={classes.container}>
        <Paper className={classes.cardContainer}>
          <div className={classes.infoContainer}>
            <Typography variant="h4">{user.username}</Typography>
            <div className={classes.detailInfo}>
              <Typography>Prénom:</Typography>
              <Typography className={classes.value}>
                {user.first_name || "-"}
              </Typography>
              <Typography>Nom:</Typography>
              <Typography className={classes.value}>
                {user.last_name || "-"}
              </Typography>
              <Typography>Email:</Typography>
              <Typography className={classes.value}>
                {user.email || "-"}
              </Typography>
            </div>
          </div>

          <Avatar
            alt={user.username}
            src={`${process.env.REACT_APP_API_URI}${user?.photo}`}
            className={classes.avatar}
          />
        </Paper>
        <Button
          variant="outlined"
          color="secondary"
          onClick={() => setOpenConfirmModal(true)}
          className={classes.button}
        >
          Retirer de mes amis
        </Button>
        {renderConfirmModal()}
      </div>
    );
  }
  return (
    <div className={classes.container}>
      <Paper className={classes.cardContainerEmpty}>
        <EmptyIcon className={classes.svg} />
        <Typography className={classes.emptyText} variant="subtitle1">
          Aucun ami séléctionné
        </Typography>
      </Paper>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  button: {
    marginTop: theme.spacing(),
  },
  svg: {
    width: "150px",
    fill: "grey",
    opacity: 0.5,
    marginBottom: theme.spacing(1),
  },
  emptyText: {
    color: "grey",
    fontWeight: "bold",
  },
  fieldContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginTop: theme.spacing(3),
  },
  value: {
    marginLeft: theme.spacing(),
    fontWeight: "bold",
  },
  infoContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-around",
  },
  detailInfo: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gridRowGap: theme.spacing(3),
    gridColumnGap: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  cardContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: "5%",
    height: "300px",
    background: theme.palette.primary.light,

    width: "95%",
  },
  cardContainerEmpty: {
    display: "flex",
    background: "#ececec",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    paddingBottom: "25px",
    paddingTop: "25px",
    height: "250px",
    width: "95%",
  },

  avatar: {
    width: "12vw",
    height: "12vw",
    marginRight: theme.spacing(10),
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    width: "50%",
  },
}));

export default UserCard;

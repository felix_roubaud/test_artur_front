import React, { useState, useRef } from "react";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import LockIcon from "@material-ui/icons/Lock";
import { Button, TextField, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

function UserLoginForm({ loginUser, error }) {
  let history = useHistory();
  const classes = useStyles();
  const passwordRef = useRef();
  const userNameRef = useRef();
  const loginButttonRef = useRef();
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");

  async function login() {
    loginUser(username, password);
  }

  function keyPress(e) {
    if (e.keyCode === 13 && !!username && !!password) {
      loginButttonRef.current.focus();
    } else if (e.keyCode === 13 && !!username) {
      passwordRef.current.focus();
    }
  }

  return (
    <div className={classes.container}>
      <TextField
        value={username}
        inputRef={userNameRef}
        className={classes.textField}
        autoFocus
        onKeyDown={keyPress}
        onChange={(ev) => {
          setUserName(ev.target.value);
        }}
        variant="outlined"
        placeholder="Nom d'utilisateur"
        icon={<AccountCircleIcon />}
        required
      />

      <TextField
        className={classes.textField}
        value={password}
        variant="outlined"
        onKeyDown={keyPress}
        inputRef={passwordRef}
        onChange={(ev) => {
          setPassword(ev.target.value);
        }}
        placeholder="Mot de passe"
        icon={<LockIcon />}
        required
        password
      />
      {error &&
        error.map((err) => <Typography color="secondary">{err}</Typography>)}
      <Button
        variant="contained"
        className={classes.login}
        onClick={login}
        color="primary"
        ref={loginButttonRef}
      >
        Login
      </Button>
      <div className={classes.a} onClick={() => history.push("/signup")}>
        Pas encore de compte ?{" "}
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  login: {
    padding: theme.spacing(),
    width: "100%",
    backgroundColor: theme.palette.primary,
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  textField: {
    marginBottom: theme.spacing(2),
    width: "100%",
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "200px",
    marginRight: "120px",
  },
  a: {
    color: "unset",
    textDecoration: "underline",
    cursor: "pointer",
  },
}));

export default UserLoginForm;

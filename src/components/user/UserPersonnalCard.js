import React from "react";
import { Typography, CircularProgress, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

function UserPersonnalCard({ user }) {
  const classes = useStyles();
  if (user) {
    return (
      <Paper className={classes.container}>
        <Typography variant="h4">{user.username}</Typography>
        <div className={classes.detailInfo}>
          <Typography>Prénom:</Typography>
          <Typography className={classes.value}>
            {user.first_name || "-"}
          </Typography>
          <Typography>Nom:</Typography>
          <Typography className={classes.value}>
            {user.last_name || "-"}
          </Typography>
          <Typography>Email:</Typography>
          <Typography className={classes.value}>{user.email || "-"}</Typography>
        </div>
      </Paper>
    );
  }
  return (
    <Paper className={classes.container}>
      <CircularProgress />
    </Paper>
  );
}

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-around",
    padding: theme.spacing(4),
    background: "#ececec",
  },
  fieldContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    marginTop: theme.spacing(6),
  },
  detailInfo: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr",
    gridRowGap: theme.spacing(5),
    gridColumnGap: theme.spacing(3),
    marginTop: theme.spacing(3),
  },
  value: {
    marginLeft: theme.spacing(),
    fontWeight: "bold",
  },
}));

export default UserPersonnalCard;

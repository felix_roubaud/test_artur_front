import React from "react";
import {
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
} from "@material-ui/core";

function UserListItem({ user, onClick, selected, divider }) {
  return (
    <ListItem
      divider={divider}
      button={!!onClick}
      onClick={onClick ? () => onClick(user.id) : null}
      selected={selected}
    >
      <ListItemAvatar>
        <Avatar
          alt={user.username}
          src={`${process.env.REACT_APP_API_URI}${user?.photo}`}
        />
      </ListItemAvatar>
      <ListItemText primary={user.username} secondary={user?.email || "-"} />
    </ListItem>
  );
}

export default UserListItem;

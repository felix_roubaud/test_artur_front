import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { Avatar, IconButton } from "@material-ui/core";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

import { useAuth } from "context/auth";
import { ReactComponent as FolderSvg } from "./folders.svg";
import { ReactComponent as FirendsSvg } from "./friends.svg";

function Template({ children, user }) {
  const classes = useStyles();
  let history = useHistory();
  const { setAuthTokens } = useAuth();
  const [selected, setSelected] = useState(() => {
    if (history.location.pathname.includes("files")) {
      return "files";
    } else if (history.location.pathname.includes("friends")) {
      return "friends";
    } else {
      return null;
    }
  });

  history.listen((location, action) => {
    if (history.location.pathname.includes("files")) {
      setSelected("files");
    } else if (history.location.pathname.includes("friends")) {
      setSelected("friends");
    } else {
      setSelected(null);
    }
  });

  return (
    <div className={classes.container}>
      <div className={classes.leftBar}>
        <div
          className={
            selected === "files"
              ? classes.selectedCard
              : classes.nonSelectedCard
          }
        >
          <FolderSvg
            width="150px"
            height="150px"
            onClick={() => {
              history.push("/files");
            }}
          />
        </div>
        <div
          className={`${
            selected === "friends"
              ? classes.selectedCard
              : classes.nonSelectedCard
          } ${classes.bottomCard}`}
        >
          <FirendsSvg
            onClick={() => {
              history.push("/friends");
            }}
            width="150px"
            height="150px"
            className={classes.svg}
          />
        </div>
      </div>
      <div className={classes.rightContainer}>
        <div className={classes.topBar}>
          <Avatar
            onClick={() => {
              history.push("/account");
            }}
            className={classes.avatar}
            alt={user?.username}
            src={`${process.env.REACT_APP_API_URI}${user?.photo}`}
          />
          <IconButton
            onClick={() => {
              setAuthTokens(null);
              history.push("/login");
            }}
          >
            <ExitToAppIcon className={classes.exitIcon} />
          </IconButton>
        </div>
        <div className={classes.contentContainer}>{children}</div>
      </div>
    </div>
  );
}

const useStyles = makeStyles((theme) => ({
  rightContainer: {
    display: "flex",
    width: "110%",
    marginLeft: "-5px",
    flexDirection: "column",
    alignItems: "flex-start",
    height: "100vh",
  },
  selectedCard: {
    cursor: "pointer",
    padding: "20px",
    borderRadius: "10px",
    backgroundColor: "rgb(136 181 193 / 50%)",
    boxShadow: "inset 0px 0px 10px 1px rgb(33, 116, 138) ",
  },
  nonSelectedCard: {
    cursor: "pointer",
    padding: "20px",
    borderRadius: "10px",
  },
  bottomCard: {
    marginTop: "50px",
  },
  exitIcon: {
    fontSize: "40px",
    color: "white",
  },
  contentContainer: {
    padding: "2%",
    width: "96%",
    height: "100%",
    overflowY: "hidden",
  },
  leftBar: {
    flexDirection: "column",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: theme.palette.primary.light,
    width: "25vw",
    zIndex: "1",
    height: "100vh",
  },
  topBar: {
    backgroundColor: theme.palette.primary.main,
    width: "100%",
    height: "10vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  avatar: {
    marginRight: "15px",
    width: "45px",
    height: "45px",
    cursor: "pointer",
  },
  container: {
    display: "flex",
    alignItems: "flex-start",
  },
}));

export default Template;

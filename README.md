<h1>Getting started</h1>

<h2>Setup</h2>

<p>Clone/pull/download this repository</p>
<p>Open a command line in the root directory and install project dependenciers by running:</p>

### `npm install`

<h2>Run app</h2>

<p>Open your command line in the root directory and run:</p>

### `npm start`